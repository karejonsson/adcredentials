package se.prv.adcredentials;

import se.prv.adcredentials.messages.MessageTreeConnectAndX;
import se.prv.adcredentials.messages.MessageTreeConnectAndXResponse;
import se.prv.adcredentials.network.UniAddress;
import se.prv.adcredentials.session.NtlmPasswordAuthentication;
import se.prv.adcredentials.session.SessionSetup;
import se.prv.adcredentials.session.SessionTransport;

public class Credentials {
	
	public static boolean certified(String server, String domain, String username, String password, int conn_timeout) {
		try {
			final UniAddress dc = UniAddress.getByName(server);
			
			NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(domain, username, password);
	
			SessionTransport trans = SessionTransport.getSmbTransport(dc, 0, conn_timeout);
			SessionSetup session = trans.getSmbSession(auth);
			
			session.transport();
			session.connectionState = 0;
			
			session.transport.connect();	
			System.out.println("1 session="+session);			
			System.out.println("1 trans="+trans);			
	
			String unc = "\\\\" + session.transport.tconHostName + '\\' + "IPC$";
			
	        MessageTreeConnectAndXResponse response =
	                new MessageTreeConnectAndXResponse( null );
	        MessageTreeConnectAndX request =
	                new MessageTreeConnectAndX(session, unc, "?????", null);
	        session.send(request, response);
	        session.logoff(false);
			System.out.println("2 session="+session);			
			System.out.println("2 trans="+trans);			
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
}