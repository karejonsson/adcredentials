package se.prv.adcredentials.transport;

public abstract class Response {
    public long expiration;
    public boolean isReceived;
}
