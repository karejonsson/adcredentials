package se.prv.adcredentials.util;

import se.prv.adcredentials.install.Config;
import se.prv.adcredentials.install.InstallationProperties;
import se.prv.adcredentials.install.InstallationPropertyNames;
import se.prv.adcredentials.messages.MessageTransaction;
import se.prv.adcredentials.messages.MessageTransactionResponse;

public class BufferCache {

    private static final int MAX_BUFFERS = InstallationProperties.getInt(InstallationPropertyNames.pjmad_cred_smb_maxBuffers, 16 );

    static Object[] cache = new Object[MAX_BUFFERS];
    
    private static int freeBuffers = 0;

    static public byte[] getBuffer() {
        synchronized( cache ) {
            byte[] buf;

            if (freeBuffers > 0) {
                for (int i = 0; i < MAX_BUFFERS; i++) {
                    if( cache[i] != null ) {
                        buf = (byte[])cache[i];
                        cache[i] = null;
                        freeBuffers--;
                        return buf;
                    }
                }
            }

            buf = new byte[MessageTransaction.TRANSACTION_BUF_SIZE];

            return buf;
        }
    }
    
    public static void getBuffers( MessageTransaction req, MessageTransactionResponse rsp ) {
        synchronized( cache ) {
            req.setTxn_buf(getBuffer());
            rsp.setTxn_buf(getBuffer());
        }
    }
    
    static public void releaseBuffer( byte[] buf ) {
        synchronized( cache ) {
            if (freeBuffers < MAX_BUFFERS) {
                for (int i = 0; i < MAX_BUFFERS; i++) {
                    if (cache[i] == null) {
                        cache[i] = buf;
                        freeBuffers++;
                        return;
                    }
                }
            }
        }
    }
}
