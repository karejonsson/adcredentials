package se.prv.adcredentials.session;

import java.net.InetAddress;
import java.io.IOException;

import se.prv.adcredentials.constants.ProtocolConstants;
import se.prv.adcredentials.exceptions.AuthCodeInException;
import se.prv.adcredentials.exceptions.CodeInException;
import se.prv.adcredentials.messages.MessageServerBlock;
import se.prv.adcredentials.messages.MessageLogoffAndX;
import se.prv.adcredentials.messages.MessageSessionSetupAndX;
import se.prv.adcredentials.messages.MessageSessionSetupAndXResponse;
import se.prv.adcredentials.messages.MessageTreeConnectAndX;
import se.prv.adcredentials.netbios.NbtAddress;
import se.prv.adcredentials.network.UniAddress;
import se.prv.adcredentials.util.NtStatus;
import se.prv.adcredentials.util.SigningDigest;

public final class SessionSetup {

	static NbtAddress[] dc_list = null;
	static long dc_list_expiration;
	static int dc_list_counter;

	/* 0 - not connected
	 * 1 - connecting
	 * 2 - connected
	 * 3 - disconnecting
	 */
	public int connectionState;
	int uid;
	//Vector trees;
	// Transport parameters allows trans to be removed from CONNECTIONS
	private UniAddress address;
	private int port, localPort;
	private InetAddress localAddr;

	public SessionTransport transport = null;
	private NtlmPasswordAuthentication auth;
	long expiration;
	String netbiosName = null;

	SessionSetup( UniAddress address, int port,
			InetAddress localAddr, int localPort,
			NtlmPasswordAuthentication auth ) {
		this.address = address;
		this.port = port;
		this.localAddr = localAddr;
		this.localPort = localPort;
		this.setAuth(auth);
		//trees = new Vector();
		connectionState = 0;
	}

	boolean matches( NtlmPasswordAuthentication auth ) {
		return this.getAuth() == auth || this.getAuth().equals( auth );
	}
	public synchronized SessionTransport transport() {
		if( transport == null ) {
			transport = SessionTransport.getSmbTransport( address, port, localAddr, localPort, null );
		}
		return transport;
	}
	public void send( MessageServerBlock request,
			MessageServerBlock response ) throws CodeInException {
		synchronized (transport()) {
			if( response != null ) {
				response.setReceived(false);
			}

			expiration = System.currentTimeMillis() + SessionTransport.SO_TIMEOUT;
			sessionSetup(request, response);
			if( response != null && response.isReceived() ) {
				return;
			}

			if (request instanceof MessageTreeConnectAndX) {
				MessageTreeConnectAndX tcax = (MessageTreeConnectAndX)request;
				if (netbiosName != null && tcax.getPath().endsWith("\\IPC$")) {
					/* Some pipes may require that the hostname in the tree connect
					 * be the netbios name. So if we have the netbios server name
					 * from the NTLMSSP type 2 message, and the share is IPC$, we
					 * assert that the tree connect path uses the netbios hostname.
					 */
					tcax.setPath("\\\\" + netbiosName + "\\IPC$");
				}
			}

			request.setUid(uid);
			request.setAuth(getAuth());
			try {
				transport.send( request, response );
			} catch (CodeInException se) {
				if (request instanceof MessageTreeConnectAndX) {
					logoff(true);
				}
				request.setDigest(null);
				throw se;
			}
		}
	}

	void sessionSetup( MessageServerBlock andx,
			MessageServerBlock andxResponse ) throws CodeInException {
		synchronized (transport()) {
			NtlmContext nctx = null;
			CodeInException ex = null;
			MessageSessionSetupAndX request;
			MessageSessionSetupAndXResponse response;
			byte[] token = new byte[0];
			int state = 10;

			while (connectionState != 0) {
				if (connectionState == 2 || connectionState == 3) {// connected or disconnecting
					return;
				}
				try {
					transport.wait();
				} catch (InterruptedException ie) {
					throw new CodeInException(ie.getMessage(), ie);
				}
			}
			connectionState = 1; // trying ...

			try {
				transport.connect();
				/*
				 * Session Setup And X Request / Response
				 */

				if( transport.log.level >= 4 )
					transport.log.println( "sessionSetup: accountName=" + getAuth().username + ",primaryDomain=" + getAuth().domain );

				/* We explicitly set uid to 0 here to prevent a new
				 * SMB_COM_SESSION_SETUP_ANDX from having it's uid set to an
				 * old value when the session is re-established. Otherwise a
				 * "The parameter is incorrect" error can occur.
				 */
				uid = 0;

				do {
					switch (state) {
					case 10: /* NTLM */
						if (getAuth() != NtlmPasswordAuthentication.ANONYMOUS &&
						transport.hasCapability(ProtocolConstants.CAP_EXTENDED_SECURITY)) {
							state = 20; /* NTLMSSP */
							break;
						}

						request = new MessageSessionSetupAndX( this, andx, getAuth() );
						response = new MessageSessionSetupAndXResponse( andxResponse );

						request.setAuth(getAuth());

						try {
							transport.send( request, response );
						} catch (AuthCodeInException sae) {
							throw sae;
						} catch (CodeInException se) {
							ex = se;
						}

						if( response.isLoggedInAsGuest() &&
								"GUEST".equalsIgnoreCase( getAuth().username ) == false &&
								transport.server.security != ProtocolConstants.SECURITY_SHARE &&
								getAuth() != NtlmPasswordAuthentication.ANONYMOUS) {
							throw new AuthCodeInException( NtStatus.NT_STATUS_LOGON_FAILURE );
						}

						if (ex != null)
							throw ex;

						uid = response.getUid();

						if( request.getDigest() != null ) {
							// success - install the signing digest 
							transport.digest = request.getDigest();
						}

						connectionState = 2;    

						state = 0;

						break;
					case 20:
						if (nctx == null) {
							boolean doSigning = (transport.flags2 & MessageServerBlock.FLAGS2_SECURITY_SIGNATURES) != 0;
							nctx = new NtlmContext(getAuth(), doSigning);
						}

						if (SessionTransport.log.level >= 4)
							SessionTransport.log.println(nctx);

						if (nctx.isEstablished()) {

							netbiosName = nctx.getNetbiosName();

							connectionState = 2;

							state = 0;
							break;
						}

						try {
							token = nctx.initSecContext(token, 0, token.length);
						} catch (CodeInException se) {
							/* We must close the transport or the server will be expecting a
							 * Type3Message. Otherwise, when we send a Type1Message it will return
							 * "Invalid parameter".
							 */
							try { transport.disconnect(true); } catch (IOException ioe) {}
							uid = 0;
							throw se;
						}

						if (token != null) {
							request = new MessageSessionSetupAndX(this, null, token);
							response = new MessageSessionSetupAndXResponse(null);

							if (transport.isSignatureSetupRequired( getAuth() )) {
								byte[] signingKey = nctx.getSigningKey();
								if (signingKey != null)
									request.setDigest(new SigningDigest(signingKey, true));
							}

							request.setUid(uid);
							uid = 0;

							try {
								transport.send( request, response );
							} catch (AuthCodeInException sae) {
								throw sae;
							} catch (CodeInException se) {
								ex = se;
								/* Apparently once a successfull NTLMSSP login occurs, the
								 * server will return "Access denied" even if a logoff is
								 * sent. Unfortunately calling disconnect() doesn't always
								 * actually shutdown the connection before other threads
								 * have committed themselves (e.g. InterruptTest example).
								 */
								try { transport.disconnect(true); } catch (Exception e) {}
							}

							if( response.isLoggedInAsGuest() &&
									"GUEST".equalsIgnoreCase( getAuth().username ) == false) {
								throw new AuthCodeInException( NtStatus.NT_STATUS_LOGON_FAILURE );
							}

							if (ex != null)
								throw ex;

							uid = response.getUid();

							if (request.getDigest() != null) {
								/* success - install the signing digest */
								transport.digest = request.getDigest();
							}

							token = response.getBlob();
						}

						break;
					default:
						throw new CodeInException("Unexpected session setup state: " + state);
					}
				} while (state != 0);
			} catch (CodeInException se) {
				logoff(true);
				connectionState = 0;
				throw se;
			} finally {
				transport.notifyAll();
			}
		}
	}
	
	public void logoff(boolean inError) {
		synchronized (transport()) {

			if (connectionState != 2) {
				return;
			}
			connectionState = 3; // disconnecting

			netbiosName = null;

			if( !inError && transport.server.security != MessageServerBlock.SECURITY_SHARE ) {
				/*
				 * Logoff And X Request / Response
				 */
				MessageLogoffAndX request = new MessageLogoffAndX(null);
				request.setUid(uid);
				try {
					transport.send(request, null);
				} 
				catch(CodeInException se) {
				}
				uid = 0;
			}

			connectionState = 0;
			transport.notifyAll();
		}
	}
	public String toString() {
		NtlmPasswordAuthentication auth = getAuth();
		if(auth == null) {
			return "SmbSession[accountName=null" +
					",primaryDomain=null"+
					",uid=" + uid +
					",connectionState=" + connectionState + "]";
		}
		return "SmbSession[accountName=" + getAuth().username +
				",primaryDomain=" + getAuth().domain +
				",uid=" + uid +
				",connectionState=" + connectionState + "]";
	}

	public NtlmPasswordAuthentication getAuth() {
		return auth;
	}

	public void setAuth(NtlmPasswordAuthentication auth) {
		this.auth = auth;
	}

}
