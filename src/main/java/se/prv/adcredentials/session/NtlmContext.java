package se.prv.adcredentials.session;

import se.prv.adcredentials.constants.ProtocolConstants;
import se.prv.adcredentials.exceptions.CodeInException;
import se.prv.adcredentials.ntlmssp.*;
import se.prv.adcredentials.util.Encdec;
import se.prv.adcredentials.util.Hexdump;
import se.prv.adcredentials.util.LogStream;

public class NtlmContext {

    NtlmPasswordAuthentication auth;
    int ntlmsspFlags;
    String workstation;
    boolean isEstablished = false;
    byte[] serverChallenge = null;
    byte[] signingKey = null;
    String netbiosName = null;
    int state = 1;
    LogStream log;

    public NtlmContext(NtlmPasswordAuthentication auth, boolean doSigning) {
        this.auth = auth;
        this.ntlmsspFlags = ntlmsspFlags |
                NtlmFlags.NTLMSSP_REQUEST_TARGET |
                NtlmFlags.NTLMSSP_NEGOTIATE_NTLM2 |
                NtlmFlags.NTLMSSP_NEGOTIATE_128;
        if (doSigning) {
            this.ntlmsspFlags |= NtlmFlags.NTLMSSP_NEGOTIATE_SIGN |
                NtlmFlags.NTLMSSP_NEGOTIATE_ALWAYS_SIGN |
                NtlmFlags.NTLMSSP_NEGOTIATE_KEY_EXCH;
        }
        this.workstation = Type1Message.getDefaultWorkstation();
        log = LogStream.getInstance();
    }

    public String toString() {
        String ret = "NtlmContext[auth=" + auth +
            ",ntlmsspFlags=0x" + Hexdump.toHexString(ntlmsspFlags, 8) +
            ",workstation=" + workstation +
            ",isEstablished=" + isEstablished +
            ",state=" + state +
            ",serverChallenge=";
        if (serverChallenge == null) {
            ret += "null";
        } else {
            ret += Hexdump.toHexString(serverChallenge, 0, serverChallenge.length * 2);
        }
        ret += ",signingKey=";
        if (signingKey == null) {
            ret += "null";
        } else {
            ret += Hexdump.toHexString(signingKey, 0, signingKey.length * 2);
        }
        ret += "]";
        return ret;
    }

    public boolean isEstablished() {
        return isEstablished;
    }
    public byte[] getServerChallenge()
    {
        return serverChallenge;
    }
    public byte[] getSigningKey()
    {
        return signingKey;
    }
    public String getNetbiosName()
    {
        return netbiosName;
    }

    private String getNtlmsspListItem(byte[] type2token, int id0)
    {
        int ri = 58;

        for ( ;; ) {
            int id = Encdec.dec_uint16le(type2token, ri);
            int len = Encdec.dec_uint16le(type2token, ri + 2);
            ri += 4;
            if (id == 0 || (ri + len) > type2token.length) {
                break;
            } else if (id == id0) {
                try {
                    return new String(type2token, ri, len, ProtocolConstants.UNI_ENCODING);
                } catch (java.io.UnsupportedEncodingException uee) {
                    break;
                }
            }
            ri += len;
        }

        return null;
    }
    public byte[] initSecContext(byte[] token, int offset, int len) throws CodeInException {
        switch (state) {
            case 1:
                Type1Message msg1 = new Type1Message(ntlmsspFlags, auth.getDomain(), workstation);
                token = msg1.toByteArray();

                Hexdump.hexdump(log, token, 0, token.length);

                state++;
                break;
            case 2:
                try {
                    Type2Message msg2 = new Type2Message(token);

                    Hexdump.hexdump(log, token, 0, token.length);

                    serverChallenge = msg2.getChallenge();
                    ntlmsspFlags &= msg2.getFlags();

                    Type3Message msg3 = new Type3Message(msg2,
                                auth.getPassword(),
                                auth.getDomain(),
                                auth.getUsername(),
                                workstation,
                                ntlmsspFlags);
                    token = msg3.toByteArray();

                    Hexdump.hexdump(log, token, 0, token.length);

                    if ((ntlmsspFlags & NtlmFlags.NTLMSSP_NEGOTIATE_SIGN) != 0)
                        signingKey = msg3.getMasterKey();

                    isEstablished = true;
                    state++;
                    break;
                } catch (Exception e) {
                    throw new CodeInException(e.getMessage(), e);
                }
            default:
                throw new CodeInException("Invalid state");
        }
        return token;
    }
}
