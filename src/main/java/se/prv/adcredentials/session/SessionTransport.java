package se.prv.adcredentials.session;

import java.io.*;
import java.net.*;
import java.util.*;

import se.prv.adcredentials.constants.ProtocolConstants;
import se.prv.adcredentials.exceptions.AuthCodeInException;
import se.prv.adcredentials.exceptions.CodeInException;
import se.prv.adcredentials.install.InstallationProperties;
import se.prv.adcredentials.install.InstallationPropertyNames;
import se.prv.adcredentials.messages.MessageAndXServerBlock;
import se.prv.adcredentials.messages.MessageServerBlock;
import se.prv.adcredentials.messages.MessageBlankResponse;
import se.prv.adcredentials.messages.MessageNegotiate;
import se.prv.adcredentials.messages.MessageNegotiateResponse;
import se.prv.adcredentials.messages.MessageReadAndXResponse;
import se.prv.adcredentials.messages.MessageTransaction;
import se.prv.adcredentials.messages.MessageTransactionResponse;
import se.prv.adcredentials.network.UniAddress;
import se.prv.adcredentials.transport.*;
import se.prv.adcredentials.util.*;

public class SessionTransport extends AsynchronousTransport implements ProtocolConstants {

	static final byte[] BUF = new byte[0xFFFF];
	static final MessageNegotiate NEGOTIATE_REQUEST = new MessageNegotiate();
	static LogStream log = LogStream.getInstance();
	static HashMap dfsRoots = null;

	public static synchronized SessionTransport getSmbTransport(UniAddress address, int port) {
		return getSmbTransport(
				address, 
				port, 
				LADDR, LPORT, 
				null, 
				InstallationProperties.getInt(InstallationPropertyNames.pjmad_cred_smb_client_connTimeout, DEFAULT_CONN_TIMEOUT));
	}
	
	public static synchronized SessionTransport getSmbTransport(UniAddress address, int port, int conn_timeout) {
		return getSmbTransport( address, port, LADDR, LPORT, null, conn_timeout);
	}
	
	static synchronized SessionTransport getSmbTransport( UniAddress address, int port,
			InetAddress localAddr, int localPort, String hostName) {
		return getSmbTransport(
				address, 
				port,
				localAddr, 
				localPort, 
				hostName, 
				InstallationProperties.getInt(InstallationPropertyNames.pjmad_cred_smb_client_connTimeout, DEFAULT_CONN_TIMEOUT));
	}
	
	static synchronized SessionTransport getSmbTransport(UniAddress address, int port,
			InetAddress localAddr, int localPort, String hostName, int conn_timeout) {
		SessionTransport conn;

		synchronized( CONNECTIONS ) {
			if( SSN_LIMIT != 1 ) {
				ListIterator iter = CONNECTIONS.listIterator();
				while( iter.hasNext() ) {
					conn = (SessionTransport)iter.next();
					if( conn.matches( address, port, localAddr, localPort, hostName) &&
							( SSN_LIMIT == 0 || conn.sessions.size() < SSN_LIMIT )) {
						return conn;
					}
				}
			}

			conn = new SessionTransport( address, port, localAddr, localPort, conn_timeout);
			CONNECTIONS.add( 0, conn );
		}

		return conn;
	}

	public class ServerData {
		byte flags;
		int flags2;
		public int maxMpxCount;
		public int maxBufferSize;
		public int sessionKey;
		public int capabilities;
		public String oemDomainName;
		public int securityMode;
		public int security;
		public boolean encryptedPasswords;
		public boolean signaturesEnabled;
		public boolean signaturesRequired;
		public int maxNumberVcs;
		public int maxRawSize;
		public long serverTime;
		public int serverTimeZone;
		public int encryptionKeyLength;
		public byte[] encryptionKey;
		public byte[] guid;
		public String toString() {
			return "ServerData{flags="+flags+
		", flags2="+flags2+
		", maxMpxCount="+maxMpxCount+
		", maxBufferSize="+maxBufferSize+
		", sessionKey="+sessionKey+
		", capabilities="+capabilities+
		", oemDomainName="+oemDomainName+
		", securityMode="+securityMode+
		", security="+security+
		", encryptedPasswords="+encryptedPasswords+
		", signaturesEnabled="+signaturesEnabled+
		", signaturesRequired="+signaturesRequired+
		", maxNumberVcs="+maxNumberVcs+
		", maxRawSize="+maxRawSize+
		", serverTime="+serverTime+
		", serverTimeZone="+serverTimeZone+
		", encryptionKeyLength="+encryptionKeyLength+
		", encryptionKey="+encryptionKey+
		", guid="+guid+
		"}";
		}
	}

	InetAddress localAddr;
	int localPort;
	UniAddress address;
	Socket socket;
	int port, mid;
	OutputStream out;
	InputStream in;
	byte[] sbuf = new byte[512]; /* small local buffer */
	MessageBlankResponse key = new MessageBlankResponse();
	long sessionExpiration = System.currentTimeMillis() + SO_TIMEOUT;
	SigningDigest digest = null;
	LinkedList<SessionSetup> sessions = new LinkedList<SessionSetup>();
	public ServerData server = new ServerData();
	/* Negotiated values */
	int flags2 = FLAGS2;
	private int maxMpxCount = MAX_MPX_COUNT;
	private int snd_buf_size = SND_BUF_SIZE;
	int rcv_buf_size = RCV_BUF_SIZE;
	public int capabilities = CAPABILITIES;
	public int sessionKey = 0x00000000;
	boolean useUnicode = USE_UNICODE;
	public String tconHostName = null;
	private int conn_timeout;
	
	public String toString() {
		return "SessionTransport{"+
	"super="+super.toString()+
	", localAddr="+localAddr+
	", localPort="+localPort+
	", address="+address+
	", socket="+socket+
	", port="+port+
	", mid="+mid+
	", flags2="+flags2+
	", maxMpxCount="+maxMpxCount+
	", snd_buf_size="+snd_buf_size+
	", rcv_buf_size="+rcv_buf_size+
	", capabilities="+capabilities+
	", sessionKey="+sessionKey+
	", useUnicode="+useUnicode+
	", tconHostName="+tconHostName+
	", conn_timeout="+conn_timeout+
	"}";
	}
	
	public SessionTransport(UniAddress address, int port, InetAddress localAddr, int localPort) {
		this(address, port, localAddr, localPort, 
				InstallationProperties.getInt(InstallationPropertyNames.pjmad_cred_smb_client_connTimeout, DEFAULT_CONN_TIMEOUT)
				);
	}

	public SessionTransport(UniAddress address, int port, InetAddress localAddr, int localPort, int conn_timeout) {
		this.address = address;
		this.port = port;
		this.localAddr = localAddr;
		this.localPort = localPort;
		this.conn_timeout = conn_timeout;
	}

	synchronized SessionSetup getSmbSession() {
		return getSmbSession( new NtlmPasswordAuthentication( null, null, null ));
	}
	
	public synchronized SessionSetup getSmbSession(NtlmPasswordAuthentication auth) {
		SessionSetup ssn;
		long now;

		ListIterator iter = sessions.listIterator();
		while( iter.hasNext() ) {
			ssn = (SessionSetup)iter.next();
			if( ssn.matches( auth )) {
				ssn.setAuth(auth);
				return ssn;
			}
		}

		/* logoff old sessions */
		if (SO_TIMEOUT > 0 && sessionExpiration < (now = System.currentTimeMillis())) {
			sessionExpiration = now + SO_TIMEOUT;
			iter = sessions.listIterator();
			while( iter.hasNext() ) {
				ssn = (SessionSetup)iter.next();
				if( ssn.expiration < now ) {
					ssn.logoff( false );
				}
			}
		}

		ssn = new SessionSetup(address, port, localAddr, localPort, auth);
		ssn.transport = this;
		sessions.add(ssn);

		return ssn;
	}
	
	boolean matches(UniAddress address, int port, InetAddress localAddr, int localPort, String hostName) {
		if (hostName == null)
			hostName = address.getHostName();
		return (this.tconHostName == null || hostName.equalsIgnoreCase(this.tconHostName)) &&
				address.equals( this.address ) &&
				(port == 0 || port == this.port ||
				/* port 139 is ok if 445 was requested */
				(port == 445 && this.port == 139)) &&
				(localAddr == this.localAddr ||
				(localAddr != null &&
				localAddr.equals( this.localAddr ))) &&
				localPort == this.localPort;
	}
	
	boolean hasCapability(int cap) throws CodeInException {
		try {
			connect(RESPONSE_TIMEOUT);
		} catch( IOException ioe ) {
			throw new CodeInException( ioe.getMessage(), ioe );
		}
		return (capabilities & cap) == cap;
	}
	
	boolean isSignatureSetupRequired(NtlmPasswordAuthentication auth) {
		return ( flags2 & MessageServerBlock.FLAGS2_SECURITY_SIGNATURES ) != 0 &&
				digest == null &&
				auth != NtlmPasswordAuthentication.NULL &&
				NtlmPasswordAuthentication.NULL.equals( auth ) == false;
	}

	/*
    void ssn139() throws IOException {
        Name calledName = new Name( address.firstCalledName(), 0x20, null );
        do {

            socket = new Socket();
            if (localAddr != null)
                socket.bind(new InetSocketAddress(localAddr, localPort));
            socket.connect(new InetSocketAddress(address.getHostAddress(), 139), CONN_TIMEOUT);
            socket.setSoTimeout( SO_TIMEOUT );

            out = socket.getOutputStream();
            in = socket.getInputStream();

            SessionServicePacket ssp = new SessionRequestPacket( calledName,
                    NbtAddress.getLocalName() );
            out.write( sbuf, 0, ssp.writeWireFormat( sbuf, 0 ));
            if (readn( in, sbuf, 0, 4 ) < 4) {
                try {
                    socket.close();
                } catch(IOException ioe) {
                }
                throw new SmbException( "EOF during NetBIOS session request" );
            }
            switch( sbuf[0] & 0xFF ) {
                case SessionServicePacket.POSITIVE_SESSION_RESPONSE:
                    if( log.level >= 4 )
                        log.println( "session established ok with " + address );
                    return;
                case SessionServicePacket.NEGATIVE_SESSION_RESPONSE:
                    int errorCode = (int)( in.read() & 0xFF );
                    switch (errorCode) {
                        case NbtException.CALLED_NOT_PRESENT:
                        case NbtException.NOT_LISTENING_CALLED:
                            socket.close();
                            break;
                        default:
                            disconnect( true );
                            throw new NbtException( NbtException.ERR_SSN_SRVC, errorCode );
                    }
                    break;
                case -1:
                    disconnect( true );
                    throw new NbtException( NbtException.ERR_SSN_SRVC,
                            NbtException.CONNECTION_REFUSED );
                default:
                    disconnect( true );
                    throw new NbtException( NbtException.ERR_SSN_SRVC, 0 );
            }
        } while(( calledName.name = address.nextCalledName()) != null );

        throw new IOException( "Failed to establish session with " + address );
    }
	 */

	private void negotiate( int port, MessageServerBlock resp ) throws IOException {
		/* We cannot use Transport.sendrecv() yet because
		 * the Transport thread is not setup until doConnect()
		 * returns and we want to supress all communication
		 * until we have properly negotiated.
		 */
		synchronized (sbuf) {
			if(port == 139) {
				// ssn139()
				throw new CodeInException("SmbTransport does not handle port == 139 case");
			} 
			if (port == 0) {
				port = DEFAULT_PORT; // 445
			}

			socket = new Socket();
			if (localAddr != null)
				socket.bind(new InetSocketAddress(localAddr, localPort));
			socket.connect(new InetSocketAddress(address.getHostAddress(), port), conn_timeout);
			socket.setSoTimeout( SO_TIMEOUT );

			out = socket.getOutputStream();
			in = socket.getInputStream();

			if (++mid == 32000) mid = 1;
			NEGOTIATE_REQUEST.setMid(mid);
			int n = NEGOTIATE_REQUEST.encode( sbuf, 4 );
			Encdec.enc_uint32be( n & 0xFFFF, sbuf, 0 ); /* 4 byte ssn msg header */

			Hexdump.hexdump( log, sbuf, 4, n );

			out.write( sbuf, 0, 4 + n );
			out.flush();
			// Note the Transport thread isn't running yet so we can
			// read from the socket here.
			if (peekKey() == null) /* try to read header */
				throw new IOException( "transport closed in negotiate" );
			int size = Encdec.dec_uint16be( sbuf, 2 ) & 0xFFFF;
			if (size < 33 || (4 + size) > sbuf.length ) {
				throw new IOException( "Invalid payload size: " + size );
			}
			readn( in, sbuf, 4 + 32, size - 32 );
			resp.decode( sbuf, 4 );

			Hexdump.hexdump( log, sbuf, 4, n );
		}
	}
	
	public void connect() throws CodeInException {
		try {
			super.connect( RESPONSE_TIMEOUT );
		} catch( TransportException te ) {
			throw new CodeInException( "Failed to connect: " + address, te );
		}
	}
	
	protected void doConnect() throws IOException {
		/*
		 * Negotiate Protocol Request / Response
		 */

		MessageNegotiateResponse resp = new MessageNegotiateResponse( server );
		try {
			negotiate(port, resp);
		} 
		catch(ConnectException ce) {
			port = (port == 0 || port == DEFAULT_PORT) ? 139 : DEFAULT_PORT;
			negotiate( port, resp );
		} 
		catch(NoRouteToHostException nr) {
			port = (port == 0 || port == DEFAULT_PORT) ? 139 : DEFAULT_PORT;
			negotiate( port, resp );
		}
		if(resp.getDialectIndex() > 10) {
			throw new CodeInException( "This client does not support the negotiated dialect." );
		}
		if((server.capabilities & CAP_EXTENDED_SECURITY) != CAP_EXTENDED_SECURITY &&
				server.encryptionKeyLength != 8 &&
				LM_COMPATIBILITY == 0) {
			throw new CodeInException("Unexpected encryption key length: " + server.encryptionKeyLength);
		}

		/* Adjust negotiated values */

		tconHostName = address.getHostName();
		if (server.signaturesRequired || (server.signaturesEnabled && SIGNPREF)) {
			flags2 |= MessageServerBlock.FLAGS2_SECURITY_SIGNATURES;
		} else {
			flags2 &= 0xFFFF ^ MessageServerBlock.FLAGS2_SECURITY_SIGNATURES;
		}
		setMaxMpxCount(Math.min( getMaxMpxCount(), server.maxMpxCount ));
		if (getMaxMpxCount() < 1) setMaxMpxCount(1);
		setSnd_buf_size(Math.min( getSnd_buf_size(), server.maxBufferSize ));
		capabilities &= server.capabilities;
		if ((server.capabilities & CAP_EXTENDED_SECURITY) == CAP_EXTENDED_SECURITY)
			capabilities |= CAP_EXTENDED_SECURITY; // & doesn't copy high bit

		if ((capabilities & MessageServerBlock.CAP_UNICODE) == 0) {
			// server doesn't want unicode
			if (FORCE_UNICODE) {
				capabilities |= MessageServerBlock.CAP_UNICODE;
			} else {
				useUnicode = false;
				flags2 &= 0xFFFF ^ MessageServerBlock.FLAGS2_UNICODE;
			}
		}
	}
	
	protected void doDisconnect( boolean hard ) throws IOException {
		ListIterator iter = sessions.listIterator();
		try {
			while (iter.hasNext()) {
				SessionSetup ssn = (SessionSetup)iter.next();
				ssn.logoff( hard );
			}
			socket.shutdownOutput();
			out.close();
			in.close();
			socket.close();
		} 
		finally {
			digest = null;
			socket = null;
			tconHostName = null;
		}
	}

	protected void makeKey(Request request) throws IOException {
		/* The request *is* the key */
		if (++mid == 32000) mid = 1;
		((MessageServerBlock)request).setMid(mid);
	}
	
	protected Request peekKey() throws IOException {
		int n;
		do {
			if ((n = readn( in, sbuf, 0, 4 )) < 4)
				return null;
		} while (sbuf[0] == (byte)0x85);  /* Dodge NetBIOS keep-alive */
		/* read smb header */
		if ((n = readn( in, sbuf, 4, 32 )) < 32)
			return null;
		//System.out.println( "New data read: " + this );
		Hexdump.hexdump( log, sbuf, 4, 32 );

		for ( ;; ) {
			/* 01234567
			 * 00SSFSMB
			 * 0 - 0's
			 * S - size of payload
			 * FSMB - 0xFF SMB magic #
			 */

			if (sbuf[0] == (byte)0x00 &&
					sbuf[1] == (byte)0x00 &&
					sbuf[4] == (byte)0xFF &&
					sbuf[5] == (byte)'S' &&
					sbuf[6] == (byte)'M' &&
					sbuf[7] == (byte)'B') {
				break; /* all good */
			}
			/* out of phase maybe? */
			/* inch forward 1 byte and try again */
			for (int i = 0; i < 35; i++) {
				sbuf[i] = sbuf[i + 1];
			}
			int b;
			if ((b = in.read()) == -1) return null;
			sbuf[35] = (byte)b;
		}

		key.setMid(Encdec.dec_uint16le( sbuf, 34 ) & 0xFFFF);

		/* Unless key returned is null or invalid Transport.loop() always
		 * calls doRecv() after and no one else but the transport thread
		 * should call doRecv(). Therefore it is ok to expect that the data
		 * in sbuf will be preserved for copying into BUF in doRecv().
		 */

		return key;
	}

	protected void doSend( Request request ) throws IOException {
		synchronized (BUF) {
			MessageServerBlock smb = (MessageServerBlock)request;
			int n = smb.encode( BUF, 4 );
			Encdec.enc_uint32be( n & 0xFFFF, BUF, 0 ); /* 4 byte session message header */
			if (log.level >= 4) {
				do {
					log.println( smb );
				} while (smb instanceof MessageAndXServerBlock &&
						(smb = ((MessageAndXServerBlock)smb).getAndx()) != null);
				if (LogStream.level >= 6) {
					Hexdump.hexdump( log, BUF, 4, n );
				}
			}
			/* For some reason this can sometimes get broken up into another
			 * "NBSS Continuation Message" frame according to WireShark
			 */
			out.write( BUF, 0, 4 + n );
		}
	}
	
	protected void doSend0( Request request ) throws IOException {
		try {
			doSend( request );
		} catch( IOException ioe ) {
			if (LogStream.level > 2)
				ioe.printStackTrace( log );
			try {
				disconnect( true );
			} catch( IOException ioe2 ) {
				ioe2.printStackTrace( log );
			}
			throw ioe;
		}
	}

	protected void doRecv( Response response ) throws IOException {
		MessageServerBlock resp = (MessageServerBlock)response;
		resp.setUseUnicode(useUnicode);
		resp.setExtendedSecurity((capabilities & CAP_EXTENDED_SECURITY) == CAP_EXTENDED_SECURITY);

		synchronized (BUF) {
			System.arraycopy( sbuf, 0, BUF, 0, 4 + HEADER_LENGTH );
			int size = Encdec.dec_uint16be( BUF, 2 ) & 0xFFFF;
			if (size < (HEADER_LENGTH + 1) || (4 + size) > rcv_buf_size ) {
				throw new IOException( "Invalid payload size: " + size );
			}
			int errorCode = Encdec.dec_uint32le( BUF, 9 ) & 0xFFFFFFFF;
			if (resp.command == MessageServerBlock.SMB_COM_READ_ANDX &&
					(errorCode == 0 ||
					errorCode == 0x80000005)) { // overflow indicator normal for pipe
				MessageReadAndXResponse r = (MessageReadAndXResponse)resp;
				int off = HEADER_LENGTH;
				/* WordCount thru dataOffset always 27 */
				readn( in, BUF, 4 + off, 27 ); off += 27;
				resp.decode( BUF, 4 );
				/* EMC can send pad w/o data */
				int pad = r.getDataOffset() - off;
				if (r.getByteCount() > 0 && pad > 0 && pad < 4)
					readn( in, BUF, 4 + off, pad);

				if (r.getDataLength() > 0)
					readn( in, r.b, r.off, r.getDataLength() );  /* read direct */
			} else {
				readn( in, BUF, 4 + 32, size - 32 );
				resp.decode( BUF, 4 );
				if (resp instanceof MessageTransactionResponse) {
					((MessageTransactionResponse)resp).nextElement();
				}
			}

			/* Verification fails (w/ W2K3 server at least) if status is not 0. This
			 * suggests MS doesn't compute the signature (correctly) for error responses
			 * (perhaps for DOS reasons).
			 */
			if (digest != null && resp.getErrorCode() == 0) {
				digest.verify( BUF, 4, resp );
			}

			if (LogStream.level >= 4) {
				log.println( response );
				if (LogStream.level >= 6) {
					Hexdump.hexdump( log, BUF, 4, size );
				}
			}
		}
	}
	
	protected void doSkip() throws IOException {
		int size = Encdec.dec_uint16be( sbuf, 2 ) & 0xFFFF;
		if (size < 33 || (4 + size) > rcv_buf_size ) {
			/* log message? */
			in.skip( in.available() );
		} else {
			in.skip( size - 32 );
		}
	}
	
	void checkStatus(MessageServerBlock req, MessageServerBlock resp) throws CodeInException {
		resp.setErrorCode(CodeInException.getStatusByCode( resp.getErrorCode() ));
		switch( resp.getErrorCode() ) {
		case NtStatus.NT_STATUS_OK:
			break;
		case NtStatus.NT_STATUS_ACCESS_DENIED:
		case NtStatus.NT_STATUS_WRONG_PASSWORD:
		case NtStatus.NT_STATUS_LOGON_FAILURE:
		case NtStatus.NT_STATUS_ACCOUNT_RESTRICTION:
		case NtStatus.NT_STATUS_INVALID_LOGON_HOURS:
		case NtStatus.NT_STATUS_INVALID_WORKSTATION:
		case NtStatus.NT_STATUS_PASSWORD_EXPIRED:
		case NtStatus.NT_STATUS_ACCOUNT_DISABLED:
		case NtStatus.NT_STATUS_ACCOUNT_LOCKED_OUT:
		case NtStatus.NT_STATUS_TRUSTED_DOMAIN_FAILURE:
			throw new AuthCodeInException( resp.getErrorCode() );
		case NtStatus.NT_STATUS_PATH_NOT_COVERED:
			throw new CodeInException( resp.getErrorCode(), null );
		case 0x80000005:  /* STATUS_BUFFER_OVERFLOW */
		break; /* normal for DCERPC named pipes */
		case NtStatus.NT_STATUS_MORE_PROCESSING_REQUIRED:
			break; /* normal for NTLMSSP */ // Kåre
		case NtStatus.NT_STATUS_DUPLICATE_NAME:
			break; /* normal for NTLMSSP */ // Kåre
		default: {
			throw new CodeInException( resp.getErrorCode(), null );
		}
		}
		if (resp.verifyFailed) {
			throw new CodeInException( "Signature verification failed." );
		}
	}
	
	void send(MessageServerBlock request, MessageServerBlock response) throws CodeInException {

		connect(); // must negotiate before we can test flags2, useUnicode, etc 

		request.setFlags2(request.getFlags2() | flags2);
		request.setUseUnicode(useUnicode);
		request.setResponse(response); // needed by sign 
		if (request.getDigest() == null)
			request.setDigest(digest); // for sign called in encode 

		try {
			if (response == null) {
				doSend0( request );
				return;
			} else if (request instanceof MessageTransaction) {
				response.command = request.command;
				MessageTransaction req = (MessageTransaction)request;
				MessageTransactionResponse resp = (MessageTransactionResponse)response;

				req.setMaxBufferSize(getSnd_buf_size());
				resp.reset();

				try {
					BufferCache.getBuffers( req, resp );

					// First request w/ interim response
					req.nextElement();
					if (req.hasMoreElements()) {
						MessageBlankResponse interim = new MessageBlankResponse();
						super.sendrecv( req, interim, RESPONSE_TIMEOUT );
						if (interim.getErrorCode() != 0) {
							checkStatus( req, interim );
						}
						req.nextElement();
					} else {
						makeKey( req );
					}

					synchronized (this) {
						response.setReceived(false);
						resp.isReceived = false;
						try {
							response_map.put( req, resp );

							// Send multiple fragments

							do {
								doSend0( req );
							} while( req.hasMoreElements() && req.nextElement() != null );

							// Receive multiple fragments

							long timeout = RESPONSE_TIMEOUT;
							resp.expiration = System.currentTimeMillis() + timeout;
							while( resp.hasMoreElements() ) {
								wait( timeout );
								timeout = resp.expiration - System.currentTimeMillis();
								if (timeout <= 0) {
									throw new TransportException( this +
											" timedout waiting for response to " +
											req );
								}
							}
							if (response.getErrorCode() != 0) {
								checkStatus( req, resp );
							}
						} catch( InterruptedException ie ) {
							throw new TransportException( ie );
						} finally {
							response_map.remove( req );
						}
					}
				} finally {
					BufferCache.releaseBuffer( req.getTxn_buf() );
					BufferCache.releaseBuffer( resp.getTxn_buf() );
				}

			} else {
				response.command = request.command;
				super.sendrecv( request, response,
						request.timeout==null ? RESPONSE_TIMEOUT : request.timeout.longValue() );
			}
		} 
		catch( CodeInException se ) {
			throw se;
		} 
		catch( IOException ioe ) {
			throw new CodeInException( ioe.getMessage(), ioe );
		}

		checkStatus( request, response );
	}

	public int getSnd_buf_size() {
		return snd_buf_size;
	}
	
	public void setSnd_buf_size(int snd_buf_size) {
		this.snd_buf_size = snd_buf_size;
	}
	
	public int getMaxMpxCount() {
		return maxMpxCount;
	}
	
	public void setMaxMpxCount(int maxMpxCount) {
		this.maxMpxCount = maxMpxCount;
	}

}

