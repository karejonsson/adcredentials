package se.prv.adcredentials.exceptions;

public class AuthCodeInException extends CodeInException {

    public AuthCodeInException( int errcode ) {
        super( errcode, null );
    }
    
}
