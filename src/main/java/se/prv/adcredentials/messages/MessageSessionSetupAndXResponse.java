package se.prv.adcredentials.messages;

public class MessageSessionSetupAndXResponse extends MessageAndXServerBlock {

    private String nativeOs = "";
    private String nativeLanMan = "";
    private String primaryDomain = "";

    private boolean isLoggedInAsGuest;
    private byte[] blob = null;

    public MessageSessionSetupAndXResponse( MessageServerBlock andx ) {
        super( andx );
    }

    int writeParameterWordsWireFormat( byte[] dst, int dstIndex ) {
        return 0;
    }
    int writeBytesWireFormat( byte[] dst, int dstIndex ) {
        return 0;
    }
    int readParameterWordsWireFormat( byte[] buffer, int bufferIndex ) {
        int start = bufferIndex;
        setLoggedInAsGuest(( buffer[bufferIndex] & 0x01 ) == 0x01 ? true : false);
        bufferIndex += 2;
        if (isExtendedSecurity()) {
            int blobLength = readInt2(buffer, bufferIndex);
            bufferIndex += 2;
            setBlob(new byte[blobLength]);
        }
        return bufferIndex - start;
    }
    int readBytesWireFormat( byte[] buffer, int bufferIndex ) {
        int start = bufferIndex;

        if (isExtendedSecurity()) {
            System.arraycopy(buffer, bufferIndex, getBlob(), 0, getBlob().length);
            bufferIndex += getBlob().length;
        }
        nativeOs = readString( buffer, bufferIndex );
        bufferIndex += stringWireLength( nativeOs, bufferIndex );
        nativeLanMan = readString( buffer, bufferIndex, start + getByteCount(), 255, isUseUnicode() );
        bufferIndex += stringWireLength( nativeLanMan, bufferIndex );
        if (!isExtendedSecurity()) {
            primaryDomain = readString(buffer, bufferIndex, start + getByteCount(), 255, isUseUnicode());
            bufferIndex += stringWireLength(primaryDomain, bufferIndex);
        }

        return bufferIndex - start;
    }
    public String toString() {
        String result = new String( "SmbComSessionSetupAndXResponse[" +
            super.toString() +
            ",isLoggedInAsGuest=" + isLoggedInAsGuest() +
            ",nativeOs=" + nativeOs +
            ",nativeLanMan=" + nativeLanMan +
            ",primaryDomain=" + primaryDomain + "]" );
        return result;
    }

	public boolean isLoggedInAsGuest() {
		return isLoggedInAsGuest;
	}

	public void setLoggedInAsGuest(boolean isLoggedInAsGuest) {
		this.isLoggedInAsGuest = isLoggedInAsGuest;
	}

	public byte[] getBlob() {
		return blob;
	}

	public void setBlob(byte[] blob) {
		this.blob = blob;
	}
    
}

