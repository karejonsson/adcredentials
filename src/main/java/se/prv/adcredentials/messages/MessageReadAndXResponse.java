package se.prv.adcredentials.messages;

public class MessageReadAndXResponse extends MessageAndXServerBlock {

	public byte[] b;
	public int off;
	int dataCompactionMode;
	private int dataLength;
	private int dataOffset;

	MessageReadAndXResponse() {
	}
	MessageReadAndXResponse( byte[] b, int off ) {
		this.b = b;
		this.off = off;
	}

	void setParam( byte[] b, int off ) {
		this.b = b;
		this.off = off;
	}
	int writeParameterWordsWireFormat( byte[] dst, int dstIndex ) {
		return 0;
	}
	int writeBytesWireFormat( byte[] dst, int dstIndex ) {
		return 0;
	}
	int readParameterWordsWireFormat( byte[] buffer, int bufferIndex ) {
		int start = bufferIndex;

		bufferIndex += 2; // reserved
		dataCompactionMode = readInt2( buffer, bufferIndex );
		bufferIndex += 4; // 2 reserved
		setDataLength(readInt2( buffer, bufferIndex ));
		bufferIndex += 2;
		setDataOffset(readInt2( buffer, bufferIndex ));
		bufferIndex += 12; // 10 reserved

		return bufferIndex - start;
	}
	int readBytesWireFormat( byte[] buffer, int bufferIndex ) {
		// handled special in SmbTransport.doRecv()
		return 0;
	}
	public String toString() {
		return new String( "SmbComReadAndXResponse[" +
				super.toString() +
				",dataCompactionMode=" + dataCompactionMode +
				",dataLength=" + getDataLength() +
				",dataOffset=" + getDataOffset() + "]" );
	}
	public int getDataOffset() {
		return dataOffset;
	}
	public void setDataOffset(int dataOffset) {
		this.dataOffset = dataOffset;
	}
	public int getDataLength() {
		return dataLength;
	}
	public void setDataLength(int dataLength) {
		this.dataLength = dataLength;
	}

}
