package se.prv.adcredentials.messages;

import se.prv.adcredentials.install.Config;
import se.prv.adcredentials.install.InstallationProperties;
import se.prv.adcredentials.install.InstallationPropertyNames;
import se.prv.adcredentials.session.NtlmPasswordAuthentication;
import se.prv.adcredentials.session.SessionSetup;
import se.prv.adcredentials.constants.ProtocolConstants;
import se.prv.adcredentials.exceptions.CodeInException;

public class MessageSessionSetupAndX extends MessageAndXServerBlock {

    private static final int BATCH_LIMIT =
    		InstallationProperties.getInt(InstallationPropertyNames.pjmad_cred_smb_client_SessionSetupAndX_TreeConnectAndX, 1);
    private static final boolean DISABLE_PLAIN_TEXT_PASSWORDS =
    		InstallationProperties.getBoolean(InstallationPropertyNames.pjmad_cred_smb_client_disablePlainTextPasswords, true );

    private byte[] lmHash, ntHash, blob = null;
    private int sessionKey, capabilities;
    private String accountName, primaryDomain;

    SessionSetup session;
    Object cred;

    public MessageSessionSetupAndX( SessionSetup session, MessageServerBlock andx, Object cred ) throws CodeInException {
        super( andx );
        command = SMB_COM_SESSION_SETUP_ANDX;
        this.session = session;
        this.cred = cred;

        sessionKey = session.transport.sessionKey;
        capabilities = session.transport.capabilities;

        if (session.transport.server.security == SECURITY_USER) {
            if (cred instanceof NtlmPasswordAuthentication) {
                NtlmPasswordAuthentication auth = (NtlmPasswordAuthentication)cred;

                if (auth == NtlmPasswordAuthentication.ANONYMOUS) {
                    lmHash = new byte[0];
                    ntHash = new byte[0];
                    capabilities &= ~ProtocolConstants.CAP_EXTENDED_SECURITY;
                } else if (session.transport.server.encryptedPasswords) {
                    lmHash = auth.getAnsiHash( session.transport.server.encryptionKey );
                    ntHash = auth.getUnicodeHash( session.transport.server.encryptionKey );
                    // prohibit HTTP auth attempts for the null session
                    if (lmHash.length == 0 && ntHash.length == 0) {
                        throw new RuntimeException("Null setup prohibited.");
                    }
                } else if( DISABLE_PLAIN_TEXT_PASSWORDS ) {
                    throw new RuntimeException( "Plain text passwords are disabled" );
                } else if( isUseUnicode() ) {
                    // plain text
                    String password = auth.getPassword();
                    lmHash = new byte[0];
                    ntHash = new byte[(password.length() + 1) * 2];
                    writeString( password, ntHash, 0 );
                } else {
                    // plain text
                    String password = auth.getPassword();
                    lmHash = new byte[(password.length() + 1) * 2];
                    ntHash = new byte[0];
                    writeString( password, lmHash, 0 );
                }
                accountName = auth.getUsername();
                if (isUseUnicode())
                    accountName = accountName.toUpperCase();
                primaryDomain = auth.getDomain().toUpperCase();
            } else if (cred instanceof byte[]) {
                blob = (byte[])cred;
            } else {
                throw new CodeInException("Unsupported credential type");
            }
        } else if (session.transport.server.security == SECURITY_SHARE) {
            if (cred instanceof NtlmPasswordAuthentication) {
                NtlmPasswordAuthentication auth = (NtlmPasswordAuthentication)cred;
                lmHash = new byte[0];
                ntHash = new byte[0];
                accountName = auth.getUsername();
                if (isUseUnicode())
                    accountName = accountName.toUpperCase();
                primaryDomain = auth.getDomain().toUpperCase();
            } else {
                throw new CodeInException("Unsupported credential type");
            }
        } else {
            throw new CodeInException("Unsupported");
        }
    }

    int getBatchLimit( byte command ) {
        return command == SMB_COM_TREE_CONNECT_ANDX ? BATCH_LIMIT : 0;
    }
    int writeParameterWordsWireFormat( byte[] dst, int dstIndex ) {
        int start = dstIndex;

        writeInt2( session.transport.getSnd_buf_size(), dst, dstIndex );
        dstIndex += 2;
        writeInt2( session.transport.getMaxMpxCount(), dst, dstIndex );
        dstIndex += 2;
        writeInt2( session.transport.VC_NUMBER, dst, dstIndex );
        dstIndex += 2;
        writeInt4( sessionKey, dst, dstIndex );
        dstIndex += 4;
        if (blob != null) {
            writeInt2( blob.length, dst, dstIndex );
            dstIndex += 2;
        } else {
            writeInt2( lmHash.length, dst, dstIndex );
            dstIndex += 2;
            writeInt2( ntHash.length, dst, dstIndex );
            dstIndex += 2;
        }
        dst[dstIndex++] = (byte)0x00;
        dst[dstIndex++] = (byte)0x00;
        dst[dstIndex++] = (byte)0x00;
        dst[dstIndex++] = (byte)0x00;
        writeInt4( capabilities, dst, dstIndex );
        dstIndex += 4;

        return dstIndex - start;
    }
    int writeBytesWireFormat( byte[] dst, int dstIndex ) {
        int start = dstIndex;

        if (blob != null) {
            System.arraycopy(blob, 0, dst, dstIndex, blob.length );
            dstIndex += blob.length;
        } else {
            System.arraycopy( lmHash, 0, dst, dstIndex, lmHash.length );
            dstIndex += lmHash.length;
            System.arraycopy( ntHash, 0, dst, dstIndex, ntHash.length );
            dstIndex += ntHash.length;
    
            dstIndex += writeString( accountName, dst, dstIndex );
            dstIndex += writeString( primaryDomain, dst, dstIndex );
        }
        dstIndex += writeString( session.transport.NATIVE_OS, dst, dstIndex );
        dstIndex += writeString( session.transport.NATIVE_LANMAN, dst, dstIndex );

        return dstIndex - start;
    }
    int readParameterWordsWireFormat( byte[] buffer, int bufferIndex ) {
        return 0;
    }
    int readBytesWireFormat( byte[] buffer, int bufferIndex ) {
        return 0;
    }
    public String toString() {
        String result = new String( "SmbComSessionSetupAndX[" +
            super.toString() +
            ",snd_buf_size=" + session.transport.getSnd_buf_size() +
            ",maxMpxCount=" + session.transport.getMaxMpxCount() +
            ",VC_NUMBER=" + session.transport.VC_NUMBER +
            ",sessionKey=" + sessionKey +
            ",lmHash.length=" + (lmHash == null ? 0 : lmHash.length) +
            ",ntHash.length=" + (ntHash == null ? 0 : ntHash.length) +
            ",capabilities=" + capabilities +
            ",accountName=" + accountName +
            ",primaryDomain=" + primaryDomain + "]" );
        return result;
    }
    
}
