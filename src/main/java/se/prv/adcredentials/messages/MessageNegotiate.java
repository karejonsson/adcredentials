package se.prv.adcredentials.messages;

import java.io.UnsupportedEncodingException;

public class MessageNegotiate extends MessageServerBlock {

    private static final String DIALECTS = "\u0002NT LM 0.12\u0000";

    public MessageNegotiate() {
        command = SMB_COM_NEGOTIATE;
        setFlags2(DEFAULT_FLAGS2);
    }

    int writeParameterWordsWireFormat( byte[] dst, int dstIndex ) {
        return 0;
    }
    int writeBytesWireFormat( byte[] dst, int dstIndex ) {
        byte[] dialects;
        try {
            dialects = DIALECTS.getBytes( "ASCII" );
        } catch( UnsupportedEncodingException uee ) {
            return 0;
        }
        System.arraycopy( dialects, 0, dst, dstIndex, dialects.length );
        return dialects.length;
    }
    int readParameterWordsWireFormat( byte[] buffer, int bufferIndex ) {
        return 0;
    }
    int readBytesWireFormat( byte[] buffer, int bufferIndex ) {
        return 0;
    }
    public String toString() {
        return new String( "SmbComNegotiate[" +
            super.toString() +
            ",wordCount="   + wordCount +
            ",dialects=NT LM 0.12]" );
    }
    
}

