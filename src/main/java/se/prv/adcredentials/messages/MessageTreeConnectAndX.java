package se.prv.adcredentials.messages;

import java.io.UnsupportedEncodingException;

import se.prv.adcredentials.install.Config;
import se.prv.adcredentials.install.InstallationProperties;
import se.prv.adcredentials.install.InstallationPropertyNames;
import se.prv.adcredentials.session.SessionSetup;
import se.prv.adcredentials.util.Hexdump;

public class MessageTreeConnectAndX extends MessageAndXServerBlock {

    private static final boolean DISABLE_PLAIN_TEXT_PASSWORDS =
    		InstallationProperties.getBoolean(InstallationPropertyNames.pjmad_cred_smb_client_disablePlainTextPasswords, true);

    private SessionSetup session;
    private boolean disconnectTid = false;
    private String service;
    private byte[] password;
    private int passwordLength;
    private String path;

    private static byte[] batchLimits = {
        1, 1, 1, 1, 1, 1, 1, 1, 0
    };
    
    public String toString() {
    	return "MessageTreeConnectAndX{"+
    			"super="+super.toString()+
    			", disconnectTid="+disconnectTid+
    			", service="+service+
    			", password="+password+
    			", passwordLength="+passwordLength+
    			", path="+path+
    			"}";
    }
    
    public MessageTreeConnectAndX(SessionSetup session, String path, String service, MessageServerBlock andx ) {
        super( andx );
        this.session = session;
        this.setPath(path);
        this.service = service;
        command = SMB_COM_TREE_CONNECT_ANDX;
    }

    int getBatchLimit( byte command ) {
        int c = (int)( command & 0xFF );
        // why isn't this just return batchLimits[c]?
        switch( c ) {
            case SMB_COM_CHECK_DIRECTORY:
                return batchLimits[0];
            case SMB_COM_CREATE_DIRECTORY:
                return batchLimits[2];
            case SMB_COM_DELETE:
                return batchLimits[3];
            case SMB_COM_DELETE_DIRECTORY:
                return batchLimits[4];
            case SMB_COM_OPEN_ANDX:
                return batchLimits[5];
            case SMB_COM_RENAME:
                return batchLimits[6];
            case SMB_COM_TRANSACTION:
                return batchLimits[7];
            case SMB_COM_QUERY_INFORMATION:
                return batchLimits[8];
        }
        return 0;
    }

    int writeParameterWordsWireFormat( byte[] dst, int dstIndex ) {

        if( session.transport.server.security == SECURITY_SHARE &&
                        ( session.getAuth().isHashesExternal() ||
                        session.getAuth().getPassword().length() > 0 )) {

            if( session.transport.server.encryptedPasswords ) {
                // encrypted
                password = session.getAuth().getAnsiHash( session.transport.server.encryptionKey );
                passwordLength = password.length;
            } else if( DISABLE_PLAIN_TEXT_PASSWORDS ) {
                throw new RuntimeException( "Plain text passwords are disabled" );
            } else {
                // plain text
                password = new byte[(session.getAuth().getPassword().length() + 1) * 2];
                passwordLength = writeString( session.getAuth().getPassword(), password, 0 );
            }
        } else {
            // no password in tree connect
            passwordLength = 1;
        }

        dst[dstIndex++] = disconnectTid ? (byte)0x01 : (byte)0x00;
        dst[dstIndex++] = (byte)0x00;
        writeInt2( passwordLength, dst, dstIndex );
        return 4;
    }
    int writeBytesWireFormat( byte[] dst, int dstIndex ) {
        int start = dstIndex;

        if( session.transport.server.security == SECURITY_SHARE &&
                        ( session.getAuth().isHashesExternal() ||
                        session.getAuth().getPassword().length() > 0 )) {
            System.arraycopy( password, 0, dst, dstIndex, passwordLength );
            dstIndex += passwordLength;
        } else {
            // no password in tree connect
            dst[dstIndex++] = (byte)0x00;
        }
        dstIndex += writeString( getPath(), dst, dstIndex );
        try {
            System.arraycopy( service.getBytes( "ASCII" ), 0, dst, dstIndex, service.length() );
        } catch( UnsupportedEncodingException uee ) {
            return 0;
        }
        dstIndex += service.length();
        dst[dstIndex++] = (byte)'\0';

        return dstIndex - start;
    }
    int readParameterWordsWireFormat( byte[] buffer, int bufferIndex ) {
        return 0;
    }
    int readBytesWireFormat( byte[] buffer, int bufferIndex ) {
        return 0;
    }
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
    
}

