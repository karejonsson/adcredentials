package se.prv.adcredentials.messages;

import java.io.UnsupportedEncodingException;

public class MessageTreeConnectAndXResponse extends MessageAndXServerBlock {

    private static final int SMB_SUPPORT_SEARCH_BITS = 0x0001;
    private static final int SMB_SHARE_IS_IN_DFS     = 0x0002;

    boolean supportSearchBits, shareIsInDfs;
    String service, nativeFileSystem = "";

    public MessageTreeConnectAndXResponse( MessageServerBlock andx ) {
        super( andx );
    }

    int writeParameterWordsWireFormat( byte[] dst, int dstIndex ) {
        return 0;
    }
    int writeBytesWireFormat( byte[] dst, int dstIndex ) {
        return 0;
    }
    int readParameterWordsWireFormat( byte[] buffer, int bufferIndex ) {
        supportSearchBits = ( buffer[bufferIndex] & SMB_SUPPORT_SEARCH_BITS ) == SMB_SUPPORT_SEARCH_BITS;
        shareIsInDfs = ( buffer[bufferIndex] & SMB_SHARE_IS_IN_DFS ) == SMB_SHARE_IS_IN_DFS;
        return 2;
    }
    int readBytesWireFormat( byte[] buffer, int bufferIndex ) {
        int start = bufferIndex;

        int len = readStringLength( buffer, bufferIndex, 32 );
        try {
            service = new String( buffer, bufferIndex, len, "ASCII" );
        } catch( UnsupportedEncodingException uee ) {
            return 0;
        }
        bufferIndex += len + 1;
        return bufferIndex - start;
    }
    public String toString() {
        String result = new String( "SmbComTreeConnectAndXResponse[" +
            super.toString() +
            ",supportSearchBits=" + supportSearchBits +
            ",shareIsInDfs=" + shareIsInDfs +
            ",service=" + service +
            ",nativeFileSystem=" + nativeFileSystem + "]" );
        return result;
    }
    
}

