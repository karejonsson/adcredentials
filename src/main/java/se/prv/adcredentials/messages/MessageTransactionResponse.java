package se.prv.adcredentials.messages;

import java.util.Enumeration;

public abstract class MessageTransactionResponse extends MessageServerBlock implements Enumeration {

    // relative to headerStart
    private static final int SETUP_OFFSET        = 61;

    private static final int DISCONNECT_TID      = 0x01;
    private static final int ONE_WAY_TRANSACTION = 0x02;

    private int pad;
    private int pad1;
    private boolean parametersDone, dataDone;

    protected int totalParameterCount;
    protected int totalDataCount;
    protected int parameterCount;
    protected int parameterOffset;
    protected int parameterDisplacement;
    protected int dataOffset;
    protected int dataDisplacement;
    protected int setupCount;
    protected int bufParameterStart;
    protected int bufDataStart;

    int dataCount;
    byte subCommand;
    boolean hasMore = true;
    boolean isPrimary = true;
    private byte[] txn_buf;

    /* for doNetEnum and doFindFirstNext */
    int status;
    int numEntries;
    //FileEntry[] results;

    MessageTransactionResponse() {
        setTxn_buf(null);
    }

    public void reset() {
        super.reset();
        bufDataStart = 0;
        isPrimary = hasMore = true; 
        parametersDone = dataDone = false;
    }
    public boolean hasMoreElements() {
        return getErrorCode() == 0 && hasMore;
    }
    public Object nextElement() {
        if( isPrimary ) {
            isPrimary = false;
        }
        return this;
    }
    int writeParameterWordsWireFormat( byte[] dst, int dstIndex ) {
        return 0;
    }
    int writeBytesWireFormat( byte[] dst, int dstIndex ) {
        return 0;
    }
    int readParameterWordsWireFormat( byte[] buffer, int bufferIndex ) {
        int start = bufferIndex;

        totalParameterCount = readInt2( buffer, bufferIndex );
        if( bufDataStart == 0 ) {
            bufDataStart = totalParameterCount;
        }
        bufferIndex += 2;
        totalDataCount = readInt2( buffer, bufferIndex );
        bufferIndex += 4; // Reserved
        parameterCount = readInt2( buffer, bufferIndex );
        bufferIndex += 2;
        parameterOffset = readInt2( buffer, bufferIndex );
        bufferIndex += 2;
        parameterDisplacement = readInt2( buffer, bufferIndex );
        bufferIndex += 2;
        dataCount = readInt2( buffer, bufferIndex );
        bufferIndex += 2;
        dataOffset = readInt2( buffer, bufferIndex );
        bufferIndex += 2;
        dataDisplacement = readInt2( buffer, bufferIndex );
        bufferIndex += 2;
        setupCount = buffer[bufferIndex] & 0xFF;
        bufferIndex += 2;
        if( setupCount != 0 ) {
            if( log.level > 2 )
                log.println( "setupCount is not zero: " + setupCount );
        }

        return bufferIndex - start;
    }
    int readBytesWireFormat( byte[] buffer, int bufferIndex ) {
        pad = pad1 = 0;
        int n;

        if( parameterCount > 0 ) {
            bufferIndex += pad = parameterOffset - ( bufferIndex - headerStart );
            System.arraycopy( buffer, bufferIndex, getTxn_buf(),
                            bufParameterStart + parameterDisplacement, parameterCount );
            bufferIndex += parameterCount;
        }
        if( dataCount > 0 ) {
            bufferIndex += pad1 = dataOffset - ( bufferIndex - headerStart );
            System.arraycopy( buffer, bufferIndex, getTxn_buf(),
                            bufDataStart + dataDisplacement, dataCount );
            bufferIndex += dataCount;
        }

        /* Check to see if the entire transaction has been
         * read. If so call the read methods.
         */

        if( !parametersDone &&
                ( parameterDisplacement + parameterCount ) == totalParameterCount) {
            parametersDone = true;
        }

        if( !dataDone && ( dataDisplacement + dataCount ) == totalDataCount) {
            dataDone = true;
        }

        if( parametersDone && dataDone ) {
            hasMore = false;
            readParametersWireFormat( getTxn_buf(), bufParameterStart, totalParameterCount );
            readDataWireFormat( getTxn_buf(), bufDataStart, totalDataCount );
        }

        return pad + parameterCount + pad1 + dataCount;
    }

    abstract int writeSetupWireFormat( byte[] dst, int dstIndex );
    abstract int writeParametersWireFormat( byte[] dst, int dstIndex );
    abstract int writeDataWireFormat( byte[] dst, int dstIndex );
    abstract int readSetupWireFormat( byte[] buffer, int bufferIndex, int len );
    abstract int readParametersWireFormat( byte[] buffer, int bufferIndex, int len );
    abstract int readDataWireFormat( byte[] buffer, int bufferIndex, int len );

    public String toString() {
        return new String( super.toString() +
            ",totalParameterCount=" + totalParameterCount +
            ",totalDataCount=" + totalDataCount +
            ",parameterCount=" + parameterCount +
            ",parameterOffset=" + parameterOffset +
            ",parameterDisplacement=" + parameterDisplacement +
            ",dataCount=" + dataCount +
            ",dataOffset=" + dataOffset +
            ",dataDisplacement=" + dataDisplacement +
            ",setupCount=" + setupCount +
            ",pad=" + pad +
            ",pad1=" + pad1 );
    }

	public byte[] getTxn_buf() {
		return txn_buf;
	}

	public void setTxn_buf(byte[] txn_buf) {
		this.txn_buf = txn_buf;
	}
    
}
