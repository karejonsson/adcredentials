package se.prv.adcredentials.messages;

public class MessageLogoffAndX extends MessageAndXServerBlock {

    public MessageLogoffAndX( MessageServerBlock andx ) {
        super( andx );
        command = SMB_COM_LOGOFF_ANDX;
    }

    int writeParameterWordsWireFormat( byte[] dst, int dstIndex ) {
        return 0;
    }
    int writeBytesWireFormat( byte[] dst, int dstIndex ) {
        return 0;
    }
    int readParameterWordsWireFormat( byte[] buffer, int bufferIndex ) {
        return 0;
    }
    int readBytesWireFormat( byte[] buffer, int bufferIndex ) {
        return 0;
    }
    public String toString() {
        return new String( "SmbComLogoffAndX[" +
            super.toString() + "]" );
    }
    
}
