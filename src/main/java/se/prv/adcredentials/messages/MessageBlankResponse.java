package se.prv.adcredentials.messages;

public class MessageBlankResponse extends MessageServerBlock {

    public MessageBlankResponse() {
    }

    int writeParameterWordsWireFormat( byte[] dst, int dstIndex ) {
        return 0;
    }
    int writeBytesWireFormat( byte[] dst, int dstIndex ) {
        return 0;
    }
    int readParameterWordsWireFormat( byte[] buffer, int bufferIndex ) {
        return 0;
    }
    int readBytesWireFormat( byte[] buffer, int bufferIndex ) {
        return 0;
    }
    public String toString() {
        return new String( "SmbComBlankResponse[" +
            super.toString() + "]" );
    }
    
}
