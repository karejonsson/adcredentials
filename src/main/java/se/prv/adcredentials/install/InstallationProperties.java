package se.prv.adcredentials.install;

import general.reuse.properties.HarddriveProperties;

public class InstallationProperties {

	private static final String pathToConfigFile_property = "prv.credentials.configfile";
	private static final String pathToConfigFile_default = "/etc/prvconfig/credentials.properties";

	private static HarddriveProperties hp = new HarddriveProperties(pathToConfigFile_property, pathToConfigFile_default);
	 
	public static Integer getInt(String propertyname) throws Exception {
		return hp.getInt(propertyname);
	}

	public static Long getLong(String propertyname) throws Exception {
		return hp.getLong(propertyname);
	}

	public static Long[] getLongArray(String propertyname) throws Exception {
		return hp.getLongArray(propertyname);
	}

	public static String getString(String propertyname) throws Exception {
		return hp.getString(propertyname);
	}

	public static String[] getStringArray(String propertyname) throws Exception {
		return hp.getStringArray(propertyname);
	}

	public static boolean getBoolean(String propertyname, final boolean defaultValue) {
		return hp.getBoolean(propertyname, defaultValue);
	}
	
	public static String getString(String propertyname, final String defaultValue) {
		return hp.getString(propertyname, defaultValue);
	}
	
	public static Integer getInt(String propertyname, final Integer defaultValue) {
		return hp.getCLPProperty(propertyname, defaultValue);
	}
		
}
