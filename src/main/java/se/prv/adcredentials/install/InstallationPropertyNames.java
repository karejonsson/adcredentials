package se.prv.adcredentials.install;

public class InstallationPropertyNames {
	
	public static final String pjmad_cred_smb_client_laddr = "pjmad.cred.smb.client.laddr";
	public static final String pjmad_cred_smb_client_lport = "pjmad.cred.smb.client.lport";
	public static final String pjmad_cred_smb_client_maxMpxCount = "pjmad.cred.smb.client.maxMpxCount";
	public static final String pjmad_cred_smb_client_snd_buf_size = "pjmad.cred.smb.client.snd_buf_size";
	public static final String pjmad_cred_smb_client_rcv_buf_size = "pjmad.cred.smb.client.rcv_buf_size";
	public static final String pjmad_cred_smb_client_useUnicode = "pjmad.cred.smb.client.useUnicode";
	public static final String pjmad_cred_smb_client_useNtStatus = "pjmad.cred.smb.client.useNtStatus";
	public static final String pjmad_cred_smb_client_signingPreferred = "pjmad.cred.smb.client.signingPreferred";
	public static final String pjmad_cred_smb_client_useNTSmbs = "pjmad.cred.smb.client.useNTSmbs";
	public static final String pjmad_cred_smb_client_useExtendedSecurity = "pjmad.cred.smb.client.useExtendedSecurity";
	public static final String pjmad_cred_smb_client_useBatching = "pjmad.cred.smb.client.useBatching";
	public static final String pjmad_cred_smb_client_flags2 = "pjmad.cred.smb.client.flags2";
	public static final String pjmad_cred_smb_client_capabilities = "pjmad.cred.smb.client.capabilities";
	public static final String pjmad_cred_smb_client_tcpNoDelay = "pjmad.cred.smb.client.tcpNoDelay";
	public static final String pjmad_cred_smb_client_responseTimeout = "pjmad.cred.smb.client.responseTimeout";
	public static final String pjmad_cred_smb_client_ssnLimit = "pjmad.cred.smb.client.ssnLimit";
	public static final String pjmad_cred_smb_client_soTimeout = "pjmad.cred.smb.client.soTimeout";
	public static final String pjmad_cred_smb_client_connTimeout = "pjmad.cred.smb.client.connTimeout";
	public static final String pjmad_cred_smb_client_SessionSetupAndX_TreeConnectAndX = "pjmad.cred.smb.client.SessionSetupAndX.TreeConnectAndX";
	public static final String pjmad_cred_smb_client_disablePlainTextPasswords = "pjmad.cred.smb.client.disablePlainTextPasswords";
	public static final String pjmad_cred_smb_client_transaction_buf_size = "pjmad.cred.smb.client.transaction_buf_size";	
	public static final String pjmad_cred_smb_client_domain = "pjmad.cred.smb.client.domain";
	public static final String pjmad_cred_smb_client_username = "pjmad.cred.smb.client.username";
	public static final String pjmad_cred_smb_client_password = "pjmad.cred.smb.client.password";
	public static final String pjmad_cred_smb_client_nativeOs = "pjmad.cred.smb.client.nativeOs";
	public static final String pjmad_cred_smb_client_nativeLanMan = "pjmad.cred.smb.client.nativeLanMan";
	public static final String pjmad_cred_smb_UNI_ENCODING = "pjmad.cred.smb.UNI_ENCODING";	
	public static final String pjmad_cred_smb_maxBuffers = "pjmad.cred.smb.maxBuffers";
	public static final String pjmad_cred_smb_lmCompatibility = "pjmad.cred.smb.lmCompatibility";
	
	public static final String pjmad_cred_encoding = "pjmad.cred.encoding";
	public static final String pjmad_cred_file_encoding = "pjmad.cred.file.encoding";	
	public static final String pjmad_cred_resolveOrder = "pjmad.cred.resolveOrder";

	public static final String pjmad_cred_netbios_lmhosts = "pjmad.cred.netbios.lmhosts";
	public static final String pjmad_cred_netbios_hostname = "pjmad.cred.netbios.hostname";
	public static final String pjmad_cred_netbios_wins	= "pjmad.cred.netbios.wins";
	public static final String pjmad_cred_netbios_cachePolicy = "pjmad.cred.netbios.cachePolicy";
	public static final String pjmad_cred_netbios_scope = "pjmad.cred.netbios.scope";
	public static final String pjmad_cred_netbios_baddr = "pjmad.cred.netbios.baddr";
	public static final String pjmad_cred_netbios_laddr = "pjmad.cred.netbios.laddr";
	public static final String pjmad_cred_netbios_snd_buf_size = "pjmad.cred.netbios.snd_buf_size";
	public static final String pjmad_cred_netbios_rcv_buf_size = "pjmad.cred.netbios.rcv_buf_size";
	public static final String pjmad_cred_netbios_soTimeout = "pjmad.cred.netbios.soTimeout";
	public static final String pjmad_cred_netbios_retryCount = "pjmad.cred.netbios.retryCount";
	public static final String pjmad_cred_netbios_retryTimeout = "pjmad.cred.netbios.retryTimeout";
	public static final String pjmad_cred_netbios_lport = "pjmad.cred.netbios.lport";
}
