package se.prv.adcredentials.install;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.StringTokenizer;

import se.prv.adcredentials.util.LogStream;

public class Config {

public static int socketCount = 0;

    private static LogStream log;
    public static Charset DEFAULT_OEM_ENCODING = StandardCharsets.UTF_8;

    public static InetAddress getInetAddress( String key, InetAddress def ) {
        String addr = InstallationProperties.getString(key, null);
        if( addr != null ) {
            try {
                def = InetAddress.getByName( addr );
            } catch( UnknownHostException uhe ) {
                if( LogStream.level > 0 ) {
                    log.println( addr );
                    uhe.printStackTrace( log );
                }
            }
        }
        return def;
    }
    
    public static InetAddress getLocalHost() {
        String addr = InstallationProperties.getString(InstallationPropertyNames.pjmad_cred_smb_client_laddr, null);

        if (addr != null) {
            try {
                return InetAddress.getByName( addr );
            } catch( UnknownHostException uhe ) {
                if( LogStream.level > 0 ) {
                    log.println( "Ignoring jcifs.smb.client.laddr address: " + addr );
                    uhe.printStackTrace( log );
                }
            }
        }

        return null;
    }

    /**
     * Retrieve an array of <tt>InetAddress</tt> created from a property
     * value containting a <tt>delim</tt> separated list of hostnames and/or
     * ipaddresses.
     */
    public static InetAddress[] getInetAddressArray( String key, String delim, InetAddress[] def ) {
        String p = InstallationProperties.getString(key, null);
        if( p != null ) {
            StringTokenizer tok = new StringTokenizer( p, delim );
            int len = tok.countTokens();
            InetAddress[] arr = new InetAddress[len];
            for( int i = 0; i < len; i++ ) {
                String addr = tok.nextToken();
                try {
                    arr[i] = InetAddress.getByName( addr );
                } catch( UnknownHostException uhe ) {
                    if( log.level > 0 ) {
                        log.println( addr );
                        uhe.printStackTrace( log );
                    }
                    return def;
                }
            }
            return arr;
        }
        return def;
    }
    
}

