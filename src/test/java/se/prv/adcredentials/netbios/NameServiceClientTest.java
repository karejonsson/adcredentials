package se.prv.adcredentials.netbios;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

public class NameServiceClientTest {
	
	public static void main(String[] args) throws UnknownHostException {
		NameServiceClient client = new NameServiceClient();
		System.out.println("ALLA "+Arrays.toString(client.getAllByName(new Name("127.0.0.1", 0x20, null), InetAddress.getLocalHost())));
		
	}

}
// new Name( host, 0x20, null )