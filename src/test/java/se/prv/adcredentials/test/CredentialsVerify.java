package se.prv.adcredentials.test;

import se.prv.adcredentials.Credentials;

public class CredentialsVerify {
	
	public static void main(String[] args) {
		boolean allOk = true;
		if(!testOneQuick("prv.se", "", "patent", "patent", 8500)) {
			System.out.println("Wrong 1");
			allOk = false;
		};
		if(testOneQuick("prv.se", "", "patent", "xxx", 1500)) {
			System.out.println("Wrong 2");
			allOk = false;
		};

		if(testOneSlow("prv.com", "", "patent", "patent", 1500)) {
			System.out.println("Wrong 3");
			allOk = false;
		};
		if(testOneSlow("prv.com", "", "patent", "xxx", 1500)) {
			System.out.println("Wrong 4");
			allOk = false;
		};
		System.out.println("Klart! allOK = "+allOk);
	}
	
	public static boolean testOneQuick(String server, String domain, String username, String password, int conn_timeout) {
		long before = System.currentTimeMillis();
		boolean outcome = Credentials.certified(server, domain, username, password, conn_timeout);
		long after = System.currentTimeMillis();
		if((after - before) > conn_timeout) {
			System.out.println("Tog för lång tid. Blev "+(after - before)+" medan max var "+conn_timeout);
		}
		System.out.println("username "+username+", server "+server+", domain "+domain+" -> "+outcome);
		return outcome;
	}

	public static boolean testOneSlow(String server, String domain, String username, String password, int conn_timeout) {
		long before = System.currentTimeMillis();
		boolean outcome = Credentials.certified(server, domain, username, password, conn_timeout);
		long after = System.currentTimeMillis();
		if((after - before) < conn_timeout) {
			System.out.println("Tog för kort tid. Blev "+(after - before)+" medan min var "+conn_timeout);
		}
		System.out.println("username "+username+", server "+server+", domain "+domain+" -> "+outcome);
		return outcome;
	}

}
