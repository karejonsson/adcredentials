package se.prv.adcredentials.test;

import se.prv.adcredentials.messages.MessageTreeConnectAndX;
import se.prv.adcredentials.messages.MessageTreeConnectAndXResponse;
import se.prv.adcredentials.network.UniAddress;
import se.prv.adcredentials.session.NtlmPasswordAuthentication;
import se.prv.adcredentials.session.SessionSetup;
import se.prv.adcredentials.session.SessionTransport;

public class QueryByPath {

	public static boolean query(String server, String domain, int conn_timeout, String path) {
		try {
			final UniAddress dc = UniAddress.getByName(server);

			NtlmPasswordAuthentication auth = new NtlmPasswordAuthentication(domain, "patent", "patent");

			SessionTransport trans = SessionTransport.getSmbTransport(dc, 0, conn_timeout);
			SessionSetup session = trans.getSmbSession(auth);
			
			session.transport();
			session.connectionState = 0;
			
			session.transport.connect();	
			System.out.println("1 session="+session);			
			System.out.println("1 trans="+trans);			
	
			String unc = path;//"\\\\" + path + '\\' + "IPC$";
			
	        MessageTreeConnectAndXResponse response =
	                new MessageTreeConnectAndXResponse( null );
	        MessageTreeConnectAndX request =
	                new MessageTreeConnectAndX(session, unc, "?????", null);
	        session.send(request, response);
	        session.logoff(false);
			System.out.println("2 session="+session);			
			System.out.println("2 trans="+trans);			
			System.out.println("2 response="+response);			
		}
		catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
